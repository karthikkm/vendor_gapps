ifeq ($(TARGET_GAPPS_ARCH),)
$(error "GAPPS: TARGET_GAPPS_ARCH is undefined")
endif

ifneq ($(TARGET_GAPPS_ARCH),arm)
ifneq ($(TARGET_GAPPS_ARCH),arm64)
$(error "GAPPS: Only arm and arm64 are allowed")
endif
endif

TARGET_MINIMAL_APPS ?= false

$(call inherit-product, vendor/gapps/common-blobs.mk)

# Include package overlays
DEVICE_PACKAGE_OVERLAYS += \
    vendor/gapps/overlay/

# framework
PRODUCT_PACKAGES += \
    com.google.android.maps \
    com.google.android.media.effects

ifeq ($(IS_PHONE),true)
PRODUCT_PACKAGES += \
    com.google.android.dialer.support
endif

# app
PRODUCT_PACKAGES += \
    GoogleContactsSyncAdapter \
    LatinIMEGooglePrebuilt \
    PrebuiltDeskClockGoogle 

# priv-app
PRODUCT_PACKAGES += \
    AndroidPlatformServices \
    ConfigUpdater \
    GoogleBackupTransport \
    GoogleOneTimeInitializer \
    GooglePackageInstaller \
    GooglePartnerSetup \
    GoogleServicesFramework \
    Phonesky \
    PrebuiltGmsCore 

ifeq ($(IS_PHONE),true)
PRODUCT_PACKAGES += \
    GoogleDialer
endif
